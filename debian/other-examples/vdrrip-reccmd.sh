#/bin/bash

# Mit diesem Script lassen sich Aufzeichnungen über die Reccmd.custom.conf
# umwandeln. Des weiteren werden Serien "Battlestar Galacatica" und "Stargate"
# automatisch mit "name-datum_-_title" umbenannt.
# Um es nutzen zu können einfach in die "/etc/vdr/command-hocks/reccmd.custom.conf"
# die folgende Zeile eintagen:
#
# Aufzeichnung in Avi umwandeln? : /usr/bin/vdrrip-reccmd.sh
#
# (c) 2007 Benjamin Brauns <BraunsB AT srtwist.net>
#

QUEUEFILE="/var/lib/vdr/plugins/queue.vdrrip";
TARGET="/var/lib/video.00/vdrrip";
LOGFILE="$TARGET/vdrrip-reccmd.log";

ZISE="700"; 		# Datei Groesse
CODEC="xvid";		# Video Format
CONTAINER="avi";	# Datei Endung
ENCODER="lame";		# Der zu verwendede Encoder
BITRATEv="2184";	# Video Bitrate
BITRATEa="96";		# Audio Bitrate (Mp3)
FILES="1";		# Anzahl dateien die erstellt werden sollen

#SVDRPSEND="/usr/bin/svdrpsend";

AUFNAME="$1";
 NAME1="${1/%\/2[0-9][0-9][0-9]\-[0-9][0-9]-[0-9][0-9].[0-9][0-9].[0-9][0-9].[0-9][0-9].[0-9][0-9].rec/}";
 NAME="${NAME1##*/}";
 TZ="_-_";		# Serien Trennzeichen
 TZ1="_-_";		# Serien Trennzeichen 1
 DATE1="${AUFNAME##*/}";
 DATE="${DATE1%.rec}";

 Y="${DATE:0:4}";	# Jahr 2007
 sY="${DATE:2:4}";	# Short Year 07
 M="${DATE:5:2}";	# Monat
 D="${DATE:8:2}";	# Tag
 h="${DATE:11:2}";	# Stunde
 m="${DATE:14:2}";	# Minute

fTIME="$h.$h";		# Formatierte Zeit
fDATE="$M.$D.$Y";	# Formatiertes Datum (Monat.Tag.Jahr| MM.TT.YYYY) US Format wegen der Sortierung!
NOW=`date`;

# in der info.vdr Datei nach Serien Suchen!
TEST=`grep -B0 .*Galactica*. "$AUFNAME/info.vdr"; grep -B0 .*Stargate*. "$AUFNAME/info.vdr";`

if [ "$TEST" != "" ]; then
  TITLE=`grep -A1 -B0 D.* "$AUFNAME/info.vdr"`
  TITLE=${TITLE#D };
  TITLE=${TITLE%%.*};
  TITLE=${TITLE// /_} # entfernern der " " im Folgen Title! 
#  echo "Serie erkannt: fuege den Namen der Folge hinzu!"; echo "";
  NAME="$NAME$TZ1$fDATE$TZ$TITLE"
else
#  echo "no matches!";
  NAME="$NAME-$DATE"
fi
#  echo $NAME;


#exit; # Exit for DEBUG

if [ -f "$TARGET/$NAME.info" ]; then
 echo $NOW \| Datei \""$TARGET/$NAME.info\"" existiert schon! >> "$LOGFILE";
 echo $NOW \| AUFTRAG ABGEBROCHEN! >> "$LOGFILE";
 echo Datei existiert schon,; echo AUFTRAG ABGEBROCHEN!;	# Bildschirm ausgabe
 exit
else
 cp "$AUFNAME/info.vdr" "$TARGET/$NAME.info";
 echo  $NOW \| "cp $AUFNAME/info.vdr" "$TARGET/$NAME.info" >> "$LOGFILE";

 echo $NOW \| $AUFNAME\;$NAME\-$fDATE\;$ZISE\;$FILES\;$CODEC\;$BITRATEv\;2\;15\;-1\;-1\;-1\;-1\;-1\;-1\;$ENCODER\;$BITRATEa\;0\;\(null\)\;0\;$CONTAINER\;0 >> "$LOGFILE"; 
 echo $AUFNAME\;$NAME\-$fDATE\;$ZISE\;$FILES\;$CODEC\;$BITRATEv\;2\;15\;-1\;-1\;-1\;-1\;-1\;-1\;$ENCODER\;$BITRATEa\;0\;\(null\)\;0\;$CONTAINER\;0 >> "$QUEUEFILE";

 echo $NOW \| Aufzeichnung wird umgewandelt >> "$LOGFILE";
 #echo Aufzeichnung wird umgewandelt!;	# Bildschirmausgabe
fi
